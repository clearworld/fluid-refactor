package com.paic.arch.jmsbroker;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.paic.arch.jmsbroker.mine.MessageBrokerFactory;
import com.paic.arch.jmsbroker.mine.exception.NoMessageReceivedException;
import com.paic.arch.jmsbroker.mine.factory.ActiveMQMessageBrokerFactory;

public class MessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";

    public static final String MESSAGE_CONTENT = "Lorem blah blah";

	private static MessageBrokerFactory BROKER_FACTORY;

    @BeforeClass
    public static void setup() throws Exception {
		BROKER_FACTORY = MessageBrokerFactory.createMessageBrokerFactory(ActiveMQMessageBrokerFactory.class);
    }

    @AfterClass
    public static void teardown() throws Exception {
		BROKER_FACTORY.stopRunningBroker();
    }

	/**
	 * 将消息发送给正在运行的代理
	 * @throws Exception
	 */
	@Test
	public void testSendsMessagesToTheRunningBroker() throws Exception {
		long messageCount = BROKER_FACTORY.sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
				.getEnqueuedMessageCountAt(TEST_QUEUE);
		assertThat(messageCount).isEqualTo(1);
	}

	/**
	 * 读取先前写入队列的消息
	 * 
	 * @throws Exception
	 */
	@Test
	public void testReadsMessagesPreviouslyWrittenToAQueue() throws Exception {
		String receivedMessage = BROKER_FACTORY.sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT).andThen()
				.retrieveASingleMessageFromTheDestination(TEST_QUEUE, MessageBrokerFactory.DEFAULT_RECEIVE_TIMEOUT);
		assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
	}

	@Test(expected = NoMessageReceivedException.class)
    public void testThrowsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
		BROKER_FACTORY.retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }
}