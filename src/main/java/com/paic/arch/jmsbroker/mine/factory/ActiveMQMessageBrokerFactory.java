package com.paic.arch.jmsbroker.mine.factory;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.Destination;
import org.apache.activemq.broker.region.DestinationStatistics;

import com.paic.arch.jmsbroker.mine.MessageBrokerFactory;

/**
 * @Desc ActiveMQ工厂类
 * @Author yehl
 * @Date 2018年3月7日
 */
public class ActiveMQMessageBrokerFactory extends MessageBrokerFactory {

	private static BrokerService brokerService;

	public static BrokerService getBrokerService() {
		if(brokerService == null) {
			brokerService = new BrokerService();
		}
		return brokerService;
	}

	@Override
	public void startBroker() throws Exception {
		super.startBroker();
		getBrokerService();
		brokerService.setPersistent(false);
		String brokerUrl = this.getBrokerUrl();
		brokerService.addConnector(brokerUrl);
		brokerService.start();
	}

	@Override
	public void stopRunningBroker() throws Exception {
		super.stopRunningBroker();
		if (brokerService == null) {
			throw new IllegalStateException("Cannot stop the broker from this API: perhaps it was started independently from this utility");
		}
		brokerService.stop();
		brokerService.waitUntilStopped();
	}

	protected ConnectionFactory connectionFactory;

	@Override
	public ConnectionFactory initConnectionFactory(String brokerUrl) throws JMSException {
		if (connectionFactory == null) {
			connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
		}
		return connectionFactory;
	}

	@Override
	public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
		DestinationStatistics statistics = null;
		Broker broker = brokerService.getRegionBroker();
		for (Destination destination : broker.getDestinationMap().values()) {
			if (destination.getName().equals(aDestinationName)) {
				statistics = destination.getDestinationStatistics();
				break;
			}
		}
		if (statistics == null)
			throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, this.getBrokerUrl()));

		return statistics.getMessages().getCount();
	}
}
