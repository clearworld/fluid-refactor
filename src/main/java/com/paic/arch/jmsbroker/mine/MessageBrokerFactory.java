package com.paic.arch.jmsbroker.mine;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;

import com.paic.arch.jmsbroker.SocketFinder;
import com.paic.arch.jmsbroker.mine.exception.NoMessageReceivedException;

/**
 * @Desc 消息代理工厂类
 * @Author yehl
 * @Date 2018年3月7日
 */
public abstract class MessageBrokerFactory {

	private static final Logger logger = getLogger(MessageBrokerFactory.class);

    private static final int ONE_SECOND = 1000;

	public static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

	private String brokerUrl; // 代理地址

    /**
     * 创建消息代理工厂类，绑定代理地址
     * @param brokerFactoryClass 消息工厂类型
     * @return
     */
	public static MessageBrokerFactory createMessageBrokerFactory(Class<? extends MessageBrokerFactory> brokerFactoryClass) {
		return createMessageBrokerFactory(brokerFactoryClass, DEFAULT_BROKER_URL_PREFIX + SocketFinder.findNextAvailablePortBetween(41616, 50000));
	}

    /**
     * 创建消息代理工厂类，绑定代理地址
     * @param brokerFactoryClass 消息工厂类型
     * @param aBrokerUrls 代理地址
     * @return
     */
	public static MessageBrokerFactory createMessageBrokerFactory(Class<? extends MessageBrokerFactory> brokerFactoryClass, String aBrokerUrl) {
		try {
			MessageBrokerFactory factory = brokerFactoryClass.getConstructor().newInstance();
			factory.setBrokerUrl(aBrokerUrl);
			factory.startBroker();
			return factory;
		} catch (Exception e) {
			logger.error("消息代理工厂类创建失败：", e);
			throw new RuntimeException(e);
		}
	}

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	/**
	 * 启动代理器（默认空实现）
	 */
	protected void startBroker() throws Exception { }
	
	/**
	 * 停止代理器（默认空实现）
	 */
	public void stopRunningBroker() throws Exception { }

	/**
	 * 发送文本消息
	 * 
	 * @param aDestinationName
	 * @param aMessageToSend
	 * @return
	 */
	public MessageBrokerFactory sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) throws Exception {
		execute(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
		return this;
	}

	/**
	 * 接收文本消息
	 * 
	 * @param aDestinationName
	 * @param aTimeout
	 * @return
	 */
	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) throws Exception {
		return execute(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }

	/**
	 * 
	 * @param brokerUrl
	 * @param aDestinationName
	 * @param callback
	 */
	private String execute(String brokerUrl, String aDestinationName, JmsCallback callback) {
		String returnValue = "";
		Connection connection = null;
		try {
			ConnectionFactory connectionFactory = initConnectionFactory(brokerUrl);
			connection = connectionFactory.createConnection();
			connection.start();
			returnValue = executeCallback(connection, aDestinationName, callback);
		} catch (JMSException jmse) {
			logger.error("failed to create connection to {}", brokerUrl);
			throw new IllegalStateException(jmse);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException jmse) {
					logger.warn("Failed to close connection to broker at []", brokerUrl);
					throw new IllegalStateException(jmse);
				}
			}
		}
		return returnValue;
	}

	/**
	 * 
	 * @param connection
	 * @param aDestinationName
	 * @param callback
	 * @return
	 */
	private String executeCallback(Connection connection, String aDestinationName, JmsCallback callback) {
		Session session = null;
		try {
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue(aDestinationName);
			return callback.performJmsFunction(session, queue);
		} catch (JMSException jmse) {
			logger.error("Failed to create session on connection {}", connection);
			throw new IllegalStateException(jmse);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (JMSException jmse) {
					logger.warn("Failed to close session {}", session);
					throw new IllegalStateException(jmse);
				}
			}
		}
	}

	/**
	 * 初始化连接
	 * @param brokerUrl
	 * @return
	 */
	public abstract ConnectionFactory initConnectionFactory(String brokerUrl) throws JMSException;

	/**
	 * 获取消息发送次数
	 * @param aDestinationName
	 * @return
	 * @throws Exception
	 */
	public abstract long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

	public interface JmsCallback {
		String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
	}

	public final MessageBrokerFactory andThen() {
		return this;
	}
}
